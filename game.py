# Guess My Birthday

import random
name = input("What is your name?")
print(f"Hello {name}, I will now guess your birthday.")


rand_month = random.randint(1, 12)
rand_year = random.randint(1900, 2022)

counter = 3
while counter > 0:
    guess = input(
        f"Is {rand_month} {rand_year} your birthday? Please type 'yes' or 'no'.")

    if guess == "yes":
        print("I knew it!")
        counter = 0
    elif guess == "no" and counter == 1:
        print("I'm out of guesses. I lose!")
        counter = 0
    elif guess == "no" and counter > 0:
        print("Drat! Let me try again!")
        counter -= 1
    else:
        print("Error. Please type yes or no.")
